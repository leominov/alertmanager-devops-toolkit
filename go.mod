module gitlab.com/leominov/alertmanager-devops-toolkit

go 1.21.0

require (
	github.com/gobwas/glob v0.2.3
	gopkg.in/yaml.v2 v2.4.0
)
